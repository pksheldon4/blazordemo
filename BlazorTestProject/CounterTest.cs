using BlazorProject.Pages;
using Bunit;

namespace BlazorTestProject
{
    public class CounterTest : TestContext
    {

        [Fact]
        public void CounterShouldInitiallyBe0()
        {
            var cut = RenderComponent<Counter>();
            cut.Find("p").MarkupMatches("<p>Current count: 0</p>");
        }

        [Fact]
        public void CounterShouldIncrementWhenClicked()
        {
            var cut = RenderComponent<Counter>();

            cut.Find("button").Click();

            cut.Find("p").MarkupMatches("<p>Current count: 1</p>");
        }

        [Fact]
        public void CounterShouldIncrementByValueWhenClicked()
        {
            var cut = RenderComponent<Counter>(parameters => parameters.Add(p => p.Value, 2));

            cut.Find("button").Click();

            cut.Find("p").MarkupMatches("<p>Current count: 2</p>");
        }
    }
}